﻿using Assets.Scripts.Data;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VoiceManager : QuackMonoBehaviour
{
    [SerializeField] private GameObject _chatterPFB;

    public byte Version = 1;

    protected override void OnAwake()
    {
        base.OnAwake();

        PhotonNetwork.autoJoinLobby = false;

        if (!PhotonNetwork.connected)
        {
            QLogger.Log("OnAwake() was called by Unity. Scene is loaded. Let's connect to the Photon Master Server. Calling: PhotonNetwork.ConnectUsingSettings();");
            PhotonNetwork.ConnectUsingSettings(Version + "." + SceneManagerHelper.ActiveSceneBuildIndex);
        }

        Broadcast.Add<RegistrationManager.SignInRequest>(handleSignInRequest);
        Broadcast.Add<DatabaseService.ActiveChatRequest>(handleActiveChatRequest);
    }

    protected override void OnDestroyObject()
    {
        base.OnDestroyObject();

        Broadcast.Remove<RegistrationManager.SignInRequest>(handleSignInRequest);
        Broadcast.Remove<DatabaseService.ActiveChatRequest>(handleActiveChatRequest);
    }
   
    public virtual void OnConnectedToMaster()
    {
        QLogger.Log("OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room. Calling: PhotonNetwork.JoinRandomRoom();");
        //PhotonNetwork.JoinRandomRoom();
    }

    #region Private Methods

    private void loadLobbyData()
    {
        var lobbyType = new TypedLobby(Client.UserData.Id, LobbyType.SqlLobby);
        PhotonNetwork.JoinLobby(lobbyType);
    }

    private void OnJoinedLobby()
    {
        QLogger.Log("Joined lobby.");
       // PhotonNetwork.Instantiate(_chatterPFB.name, Vector3.zero, Quaternion.identity, 0);
    }
    
    #endregion

    #region Event Handlers

    private void handleSignInRequest(object args)
    {
        var data = (RegistrationManager.SignInRequest)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        loadLobbyData();
    }

    private void handleActiveChatRequest(object args)
    {
        var data = (DatabaseService.ActiveChatRequest)args;

        if (data.Result == null)
        {
            return;
        }

        //foreach (var kvp in data.data)
        //{            
        //    PhotonNetwork.CreateRoom(kvp.Value);
        //}
    }

    #endregion
}
